// Copyright 2023 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"

	monitoring "cloud.google.com/go/monitoring/apiv3/v2"
	"cloud.google.com/go/monitoring/apiv3/v2/monitoringpb"
	"github.com/golang/protobuf/ptypes/timestamp"
	"github.com/google/uuid"
	"google.golang.org/api/iterator"
	"google.golang.org/genproto/googleapis/api/label"
	metricpb "google.golang.org/genproto/googleapis/api/metric"
	monitoredres "google.golang.org/genproto/googleapis/api/monitoredres"
)

// Name of the custom metric
const metricType = "custom.googleapis.com/ludo_measurement"

func main() {
	projectID := os.Getenv("PROJECT_ID")
	if projectID == "" {
		log.Fatalf("Env var PROJECT_ID not set")
	}
	var timeseriesPerRequest int
	var requestNumber int
	var reCreateMetricDescriptor bool
	flag.IntVar(&timeseriesPerRequest, "timeseriesPerRequest", 200, "number of timeseries batched per create request")
	flag.IntVar(&requestNumber, "requestNumber", 50, "number of timeseries create request")
	flag.BoolVar(&reCreateMetricDescriptor, "reCreateMD", false, "delete existing metric descriptor and create it again")
	flag.Parse()

	ctx := context.Background()
	metricClient, err := monitoring.NewMetricClient(ctx)
	if err != nil {
		log.Fatalf("monitoring.NewMetricClient %v", err)
	}
	defer metricClient.Close()

	md, err := getMetricDescriptor(ctx, metricClient, projectID, metricType)
	if err != nil {
		if strings.Contains(err.Error(), "NotFound") {
			_, err = createMetricDescriptor(ctx, metricClient, projectID, metricType)
			if err != nil {
				log.Fatalf("createMetricDescriptor %v", err)
			}
		} else {
			log.Fatalf("getMetricDescriptor %v", err)
		}
	} else {
		mdJSON, err := json.MarshalIndent(&md, "", "    ")
		if err != nil {
			log.Fatalf("json.MarshalIndent(&md %v", err)
		}
		fmt.Printf("found existing metric descriptor:\n%s\n", string(mdJSON))
		if reCreateMetricDescriptor {
			err = deleteMetricDescriptor(ctx, metricClient, projectID, metricType)
			if err != nil {
				log.Fatalf("deleteMetricDescriptor %v", err)
			}
			log.Printf("deleted existing metric descriptor")
			md, err = createMetricDescriptor(ctx, metricClient, projectID, metricType)
			if err != nil {
				log.Fatalf("createMetricDescriptor %v", err)
			}
			mdJSON, err := json.MarshalIndent(&md, "", "    ")
			if err != nil {
				log.Fatalf("json.MarshalIndent(&md %v", err)
			}
			fmt.Printf("re created existing metric descriptor:\n%s\n", string(mdJSON))
		}
	}

	for i := 0; i < requestNumber; i++ {
		r := buildTSRequest(projectID, timeseriesPerRequest)
		err = metricClient.CreateTimeSeries(ctx, r)
		if err != nil {
			log.Fatalf("metricClient.CreateTimeSeries %v", err)
		}
		fmt.Printf("%v CreateTimeSeries %d timeseriesPerRequest %d\n", time.Now(), i, timeseriesPerRequest)
	}

	timeSeriesCol, err := readTimeSeriesValue(ctx, metricClient, projectID, metricType)
	if err != nil {
		log.Fatalf("readTimeSeriesValue %v", err)
	}
	b, err := json.MarshalIndent(&timeSeriesCol, "", "    ")
	if err != nil {
		log.Fatalf("json.MarshalIndent %v", err)
	}
	err = os.WriteFile("timeSeriesCol.json", b, 0600) // prevent G306 Expect WriteFile permissions to be 0600 or less
	if err != nil {
		log.Fatalf("ioutil.WriteFile %v", err)
	}
}

func buildTSRequest(projectID string, timeseriesPerRequest int) *monitoringpb.CreateTimeSeriesRequest {
	var req monitoringpb.CreateTimeSeriesRequest
	req.Name = fmt.Sprintf("projects/%s", projectID)
	var tSeriesCol []*monitoringpb.TimeSeries
	metric := &metricpb.Metric{
		Type: metricType,
		Labels: map[string]string{
			"environment": "STAGING",
		},
	}
	now := &timestamp.Timestamp{
		Seconds: time.Now().Unix(),
	}
	points := []*monitoringpb.Point{{
		Interval: &monitoringpb.TimeInterval{
			StartTime: now,
			EndTime:   now,
		},
		Value: &monitoringpb.TypedValue{
			Value: &monitoringpb.TypedValue_Int64Value{
				Int64Value: rand.Int63n(10),
			},
		},
	}}
	for i := 0; i < timeseriesPerRequest; i++ {
		var tSeries monitoringpb.TimeSeries
		tSeries.Metric = metric
		id := uuid.New()
		tSeries.Resource = &monitoredres.MonitoredResource{
			Type: "generic_node",
			Labels: map[string]string{
				"project_id": projectID,
				"location":   "europe-west1",
				"namespace":  "bruno",
				"node_id":    id.String(),
			},
		}
		tSeries.Points = points
		tSeriesCol = append(tSeriesCol, &tSeries)
	}
	req.TimeSeries = tSeriesCol
	// log.Printf("writeTimeseriesRequest: %+v\n", req)
	return &req
}

func readTimeSeriesValue(ctx context.Context, metricClient *monitoring.MetricClient, projectID, metricType string) (timeSeriesCol []*monitoringpb.TimeSeries, err error) {
	startTime := time.Now().UTC().Add(time.Minute * -5).Unix()
	endTime := time.Now().UTC().Unix()

	// default page size is 100.000
	req := &monitoringpb.ListTimeSeriesRequest{
		Name:   "projects/" + projectID,
		Filter: fmt.Sprintf("metric.type=\"%s\"", metricType),
		Interval: &monitoringpb.TimeInterval{
			StartTime: &timestamp.Timestamp{Seconds: startTime},
			EndTime:   &timestamp.Timestamp{Seconds: endTime},
		},
	}
	iter := metricClient.ListTimeSeries(ctx, req)

	for {
		timeSeries, err := iter.Next()
		if err == iterator.Done {
			break
		}
		if err != nil {
			return timeSeriesCol, fmt.Errorf("iter.Next, %w ", err)
		}
		timeSeriesCol = append(timeSeriesCol, timeSeries)
	}

	return timeSeriesCol, nil
}

func getMetricDescriptor(ctx context.Context, metricClient *monitoring.MetricClient, projectID, metricType string) (md *metricpb.MetricDescriptor, err error) {
	req := &monitoringpb.GetMetricDescriptorRequest{
		Name: fmt.Sprintf("projects/%s/metricDescriptors/%s", projectID, metricType),
	}
	md, err = metricClient.GetMetricDescriptor(ctx, req)
	return md, err
}

func deleteMetricDescriptor(ctx context.Context, metricClient *monitoring.MetricClient, projectID, metricType string) (err error) {
	req := &monitoringpb.DeleteMetricDescriptorRequest{
		Name: fmt.Sprintf("projects/%s/metricDescriptors/%s", projectID, metricType),
	}
	err = metricClient.DeleteMetricDescriptor(ctx, req)
	return err
}

func createMetricDescriptor(ctx context.Context, metricClient *monitoring.MetricClient, projectID, metricType string) (md *metricpb.MetricDescriptor, err error) {
	md = &metricpb.MetricDescriptor{
		Name: "ludoMetric",
		Type: metricType,
		Labels: []*label.LabelDescriptor{{
			Key:         "operation",
			ValueType:   label.LabelDescriptor_STRING,
			Description: "Which DR op",
		}},
		MetricKind:  metricpb.MetricDescriptor_GAUGE,
		ValueType:   metricpb.MetricDescriptor_INT64,
		Unit:        "s",
		Description: "A useful DR measurement",
		DisplayName: "Ludo Metric",
	}
	req := &monitoringpb.CreateMetricDescriptorRequest{
		Name:             fmt.Sprintf("projects/%s", projectID),
		MetricDescriptor: md,
	}
	md, err = metricClient.CreateMetricDescriptor(ctx, req)
	return md, err
}
