# High cardinality resource label

## Goal

This POC validates there is no error (like [quotas](https://cloud.google.com/monitoring/quotas#custom_metrics_quotas) errors) when writing timeseries to Google Cloud Monitoring using:

- a user defined metric
- and the resource type [generic_node](https://cloud.google.com/monitoring/api/resources#tag_generic_node)
  - With 10k different `node_id`, aka high cardinality on one of the resource label

## Setup

- [install Go](https://go.dev/doc/install), same version as in [`go.mod`](go.mod) file
- Clone the repo
- Create a `set_env_vars.sh` script like :

```shell
#!/usr/bin/env bash


# Copyright 2023 Google LLC

# Licensed under the Apache License, Version 2.0 (the 'License');
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at

#     http://www.apache.org/licenses/LICENSE-2.0

# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an 'AS IS' BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License

# to be run as source ensuring env vars to be available after the script completes  
# source ./set_env_vars.sh

export PROJECT_ID="your-project-id"

export GOOGLE_APPLICATION_CREDENTIALS="path to a service account key with monitoring admin on the test project"
```

## Run

`go run main.go` with no argument will make 50 write request each with 200 timeseries (50*200=10k) and should result in an output like:

```shell
2023-06-21 09:23:10.234176123 +0200 CEST m=+1.219277636 CreateTimeSeries 0 timeseriesPerRequest 200
2023-06-21 09:23:10.447068951 +0200 CEST m=+1.432170415 CreateTimeSeries 1 timeseriesPerRequest 200
2023-06-21 09:23:10.726413898 +0200 CEST m=+1.711515302 CreateTimeSeries 2 timeseriesPerRequest 200
2023-06-21 09:23:10.899153249 +0200 CEST m=+1.884254725 CreateTimeSeries 3 timeseriesPerRequest 200
2023-06-21 09:23:21.027970846 +0200 CEST m=+12.013072281 CreateTimeSeries 4 timeseriesPerRequest 200
2023-06-21 09:23:21.29214609 +0200 CEST m=+12.277247553 CreateTimeSeries 5 timeseriesPerRequest 200
2023-06-21 09:23:21.419321611 +0200 CEST m=+12.404423107 CreateTimeSeries 6 timeseriesPerRequest 200
2023-06-21 09:23:21.811425677 +0200 CEST m=+12.796527140 CreateTimeSeries 7 timeseriesPerRequest 200
2023-06-21 09:23:31.9932236 +0200 CEST m=+22.978325064 CreateTimeSeries 8 timeseriesPerRequest 200
2023-06-21 09:23:42.335481573 +0200 CEST m=+33.320583021 CreateTimeSeries 9 timeseriesPerRequest 200
2023-06-21 09:23:42.655222829 +0200 CEST m=+33.640324237 CreateTimeSeries 10 timeseriesPerRequest 200
2023-06-21 09:23:52.824130411 +0200 CEST m=+43.809231925 CreateTimeSeries 11 timeseriesPerRequest 200
2023-06-21 09:23:53.183859762 +0200 CEST m=+44.168961246 CreateTimeSeries 12 timeseriesPerRequest 200
2023-06-21 09:23:53.351315608 +0200 CEST m=+44.336417074 CreateTimeSeries 13 timeseriesPerRequest 200
2023-06-21 09:23:53.519411437 +0200 CEST m=+44.504512933 CreateTimeSeries 14 timeseriesPerRequest 200
2023-06-21 09:23:53.7034028 +0200 CEST m=+44.688504264 CreateTimeSeries 15 timeseriesPerRequest 200
2023-06-21 09:23:54.039554349 +0200 CEST m=+45.024655808 CreateTimeSeries 16 timeseriesPerRequest 200
2023-06-21 09:23:54.375218322 +0200 CEST m=+45.360319729 CreateTimeSeries 17 timeseriesPerRequest 200
2023-06-21 09:23:54.543293339 +0200 CEST m=+45.528394803 CreateTimeSeries 18 timeseriesPerRequest 200
2023-06-21 09:23:54.711013634 +0200 CEST m=+45.696115039 CreateTimeSeries 19 timeseriesPerRequest 200
2023-06-21 09:23:54.879072212 +0200 CEST m=+45.864173611 CreateTimeSeries 20 timeseriesPerRequest 200
2023-06-21 09:23:55.071038996 +0200 CEST m=+46.056140399 CreateTimeSeries 21 timeseriesPerRequest 200
2023-06-21 09:23:55.231385425 +0200 CEST m=+46.216486888 CreateTimeSeries 22 timeseriesPerRequest 200
2023-06-21 09:23:55.399511542 +0200 CEST m=+46.384613007 CreateTimeSeries 23 timeseriesPerRequest 200
2023-06-21 09:24:05.630533458 +0200 CEST m=+56.615634917 CreateTimeSeries 24 timeseriesPerRequest 200
2023-06-21 09:24:05.926535454 +0200 CEST m=+56.911636937 CreateTimeSeries 25 timeseriesPerRequest 200
2023-06-21 09:24:06.094657941 +0200 CEST m=+57.079759403 CreateTimeSeries 26 timeseriesPerRequest 200
2023-06-21 09:24:06.414158152 +0200 CEST m=+57.399259632 CreateTimeSeries 27 timeseriesPerRequest 200
2023-06-21 09:24:16.629471002 +0200 CEST m=+67.614572486 CreateTimeSeries 28 timeseriesPerRequest 200
2023-06-21 09:24:16.85607869 +0200 CEST m=+67.841180092 CreateTimeSeries 29 timeseriesPerRequest 200
2023-06-21 09:24:16.978205844 +0200 CEST m=+67.963307241 CreateTimeSeries 30 timeseriesPerRequest 200
2023-06-21 09:24:17.904153361 +0200 CEST m=+68.889254824 CreateTimeSeries 31 timeseriesPerRequest 200
2023-06-21 09:24:18.082719712 +0200 CEST m=+69.067821172 CreateTimeSeries 32 timeseriesPerRequest 200
2023-06-21 09:24:18.357328345 +0200 CEST m=+69.342429814 CreateTimeSeries 33 timeseriesPerRequest 200
2023-06-21 09:24:18.52806859 +0200 CEST m=+69.513170053 CreateTimeSeries 34 timeseriesPerRequest 200
2023-06-21 09:24:29.056168472 +0200 CEST m=+80.041269938 CreateTimeSeries 35 timeseriesPerRequest 200
2023-06-21 09:24:29.328342531 +0200 CEST m=+80.313443994 CreateTimeSeries 36 timeseriesPerRequest 200
2023-06-21 09:24:29.561988398 +0200 CEST m=+80.547089795 CreateTimeSeries 37 timeseriesPerRequest 200
2023-06-21 09:24:29.971975148 +0200 CEST m=+80.957076611 CreateTimeSeries 38 timeseriesPerRequest 200
2023-06-21 09:24:30.365125805 +0200 CEST m=+81.350227272 CreateTimeSeries 39 timeseriesPerRequest 200
2023-06-21 09:24:30.718816487 +0200 CEST m=+81.703918303 CreateTimeSeries 40 timeseriesPerRequest 200
2023-06-21 09:24:40.985597625 +0200 CEST m=+91.970699088 CreateTimeSeries 41 timeseriesPerRequest 200
2023-06-21 09:24:51.266140358 +0200 CEST m=+102.251241821 CreateTimeSeries 42 timeseriesPerRequest 200
2023-06-21 09:25:01.488504025 +0200 CEST m=+112.473605488 CreateTimeSeries 43 timeseriesPerRequest 200
2023-06-21 09:25:01.920672106 +0200 CEST m=+112.905773569 CreateTimeSeries 44 timeseriesPerRequest 200
2023-06-21 09:25:02.092378667 +0200 CEST m=+113.077480127 CreateTimeSeries 45 timeseriesPerRequest 200
2023-06-21 09:25:02.316788914 +0200 CEST m=+113.301890311 CreateTimeSeries 46 timeseriesPerRequest 200
2023-06-21 09:25:02.495556193 +0200 CEST m=+113.480657590 CreateTimeSeries 47 timeseriesPerRequest 200
2023-06-21 09:25:02.667907639 +0200 CEST m=+113.653009106 CreateTimeSeries 48 timeseriesPerRequest 200
2023-06-21 09:25:02.845432823 +0200 CEST m=+113.830534285 CreateTimeSeries 49 timeseriesPerRequest 200
```

## check results

`ls` should list a `timeSeriesCol.json`, containing the retrieved 10k different values for the `nod_id` resource label, with 10k json objects like:

```json
    {
        "metric": {
            "type": "custom.googleapis.com/ludo_measurement",
            "labels": {
                "environment": "STAGING"
            }
        },
        "resource": {
            "type": "generic_node",
            "labels": {
                "location": "europe-west1",
                "namespace": "bruno",
                "node_id": "00024bc9-97be-44d4-8336-d0448011ac44",
                "project_id": "you-don-t-need-to-know-this"
            }
        },
        "metric_kind": 1,
        "value_type": 2,
        "points": [
            {
                "interval": {
                    "end_time": {
                        "seconds": 1687332301
                    },
                    "start_time": {
                        "seconds": 1687332301
                    }
                },
                "value": {
                    "Value": {
                        "Int64Value": 2
                    }
                }
            }
        ]
    },

```
